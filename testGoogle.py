from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from googleapiclient.errors import HttpError

# Credentials
scope = ['https://www.googleapis.com/auth/script.projects','https://www.googleapis.com/auth/spreadsheets']
store = file.Storage('/u6/cdb3753/Documents/AutoCaptain/credentials.json')
creds = store.get()
if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets('/u6/cdb3753/Documents/AutoCaptain/client_secret.json', scope)
    creds = tools.run_flow(flow, store)

# API Services
scriptService = build('script', 'v1', http=creds.authorize(Http()))

# Test Private Page Functions
request_private = { 'function': 'updatePrivateSheet', 'parameters':['Wingard-Brittany','Reminder','Yes'] }
scriptId_private = 'MjWpCYYeLI3W0LLxVyaGSqUQztbjGVqIl'
response_private = scriptService.scripts().run(body=request_private,scriptId=scriptId_private).execute()

# Test Public Page Functions
request_public = { 'function': 'updatePublicSheet', 'parameters':['Wingard-Brittany','5-20-2018','Available'] }
scriptId_public = 'Mr6dUfCbSHjdW7w3VCtyBeVvNKpZfdAxh'
response_public = scriptService.scripts().run(body=request_public,scriptId=scriptId_public).execute()
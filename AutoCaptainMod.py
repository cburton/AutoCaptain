#!/code/cburton/anaconda/bin/python
"""
@name: AutoCaptainMod.py
@author: Charles D. Burton
@email: burton@utexas.edu
@date: 17 May 2018
@description: Automates the position of captain for USTA teams. Schedules matches,
              gets players when needed, and emails reminders and maps. This module 
              contains definitions for many of the commonly-used functions.
"""
from __future__ import print_function
import MailHtml
from datetime import datetime,timedelta


# set the ranges for spreadsheets
schedule_range = 'Schedule!A2:K20'
contacts_range = 'Contacts!A1:F14'
emails_range = 'Emails!A1:F14'
locations_range = 'Locations!A1:B20'


class Player:
    '''Player class'''

    def __init__(self, name, info):
        self.gender = ''
        self.level = 0.0
        self.qualified = False
        self.address = ''
        self.name = str(name)
        self.firstName = str(name).split(',')[1]
        self.available = []
        for avbl in info:
            self.available.append(avbl)
        self.email = ''
        self.phone = ''

    def addContactInfo(self,contacts):
        ct_info = contacts.get(self.name)
        self.phone = ct_info[0]
        self.email = ct_info[1]
        self.address = ct_info[2]
        self.gender = ct_info[3]
        self.level = float(ct_info[4])

    def checkQual(self,participation):
        played = ["Yes","Yes - Played","1st Doubles","2nd Doubles","3rd Doubles","1st Singles","2nd Singles"]
        self.qualified = sum([p in played for p in participation])>=2


class Match:
    '''Match class'''

    def __init__(self,info):
        self.location = info[0]
        self.opponent = info[1]
        self.date = info[2]
        self.time = info[3]
        self.date_time = self.time+' '+self.date
        self.played = False
        self.sufficient = False
        self.address = ''
        self.lineup = []
        self.dt = datetime.strptime(self.date_time,'%I:%M %p %m/%d/%Y') # datetime

    def addLocation(self,addressBook):
        self.address = addressBook[self.location]


def setupGoogleAPI(serviceName, version, credentialDir):
    from apiclient.discovery import build
    from httplib2 import Http
    from oauth2client import file, client, tools
    from googleapiclient.errors import HttpError

    # Credentials
    scope = ['https://www.googleapis.com/auth/script.projects','https://www.googleapis.com/auth/spreadsheets']
    store = file.Storage(credentialDir+'/credentials.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets(credentialDir+'/client_secret.json', scope)
        creds = tools.run_flow(flow, store)

    # API Services
    service = build(serviceName, version, http=creds.authorize(Http()))
    return service


def getAvailability(serviceAPI):
    import pandas as pd
    # Call the Google API
    schedule_values = serviceAPI.spreadsheets().values().get(spreadsheetId=getKey('publicDocId'),range=schedule_range).execute()

    # Sheetsv4 does not read empty cells, so we use pandas to add these back in
    df = pd.DataFrame(schedule_values)
    df_replace = df.replace([''], [None])
    processed_dataset = df_replace.values.tolist() #convert back to list to insert into Redshift

    # convert to ASCII and append empty string to end
    availability_list = []
    for row in processed_dataset:
        availability_list.append([value for value in row[2]])
    nMatches = len(availability_list[0])
    for person in availability_list:
        while len(person) < nMatches:
            person.append('')

    # make schedule dictionary.
    schedule_list = [[availability_list[i][j] for i in range(5)] for j in range(1,nMatches)]

    # make availability dictionary. key: name, values: availability
    availability_dict = {}
    for person in availability_list[6:]:
        availability_dict[person[0]] = person[1:]

    return schedule_list,availability_dict


def getContacts(serviceAPI):
    '''Parse the player database to get the contact info.'''
    import pandas as pd
    # Call the Gooogle API
    privateId = getKey('privateDocId')
    contacts_values = serviceAPI.spreadsheets().values().get(spreadsheetId=privateId,range=contacts_range).execute()

    # Sheetsv4 does not read empty cells, so use pandas to add these back in
    df = pd.DataFrame(contacts_values)
    df_replace = df.replace([''], [None])
    processed_dataset = df_replace.values.tolist() # convert database back to list

    # convert to ASCII and append empty string to end
    contact_list = []
    for row in processed_dataset:
        contact_list.append([value for value in row[2]])
    nInfo = len(contact_list[0]) # nominal is 6: name, phone, email, address, gender, level

    # fill any empty info elements with empty strings, skipping the title row
    for person in contact_list[1:]:
        while len(person) < nInfo: person.append('')

    # turn list of name info into a dictionary with {name,info}
    contacts_dict = {}
    for person in contact_list[1:]: # start at element 1 to skip the title row
        contacts_dict[person[0]] = person[1:]

    return contacts_dict


def getEmails(serviceAPI):
    import pandas as pd
    # Call the Gooogle API
    emails_values = serviceAPI.spreadsheets().values().get(spreadsheetId=getKey('privateDocId'),range=emails_range).execute()

    # Sheetsv4 does not read empty cells, so we use pandas to add these back in
    df = pd.DataFrame(emails_values)
    df_replace = df.replace([''], [None])
    processed_dataset = df_replace.values.tolist() #convert back to list to insert into Redshift
    
    nPlayers = len(processed_dataset)-1 # height of google doc minus title row
    nEmails = len(processed_dataset[0][2])-1 # width of google doc minus name column

    # Get the data of what emails have been sent this week:
    # 1) gives the email types (Initial, Confirm, etc.)
    emailList = [processed_dataset[0][2][j+1] for j in range(nEmails)]
    # 2) gives the names of the players
    nameList = [processed_dataset[i+1][2][0] for i in range(nPlayers)]
    # 3) gives the status for each combination
    statusArray = [[processed_dataset[i+1][2][j+1]=='Yes' for j in range(nEmails)] for i in range(nPlayers)]

    playerDict = {}
    for i,name in enumerate(nameList):
        emailDict = {}
        for j,email in enumerate(emailList):
            emailDict[email] = statusArray[i][j]
        playerDict[name] = emailDict

    return playerDict


def getLocations(serviceAPI):
    import pandas as pd
    # Call the Gooogle API
    locations = serviceAPI.spreadsheets().values().get(spreadsheetId=getKey('privateDocId'),range=locations_range).execute().get('values',[])

    # make location dictionary. key: location, values: address
    loc_dict = {}
    for loc in locations[1:]:
        loc_dict[loc[0]] = loc[1]

    return loc_dict


def buildRoster(availability,contacts):
    nP = len(availability)

    # create team roster
    roster_dict = {}
    for name in availability:
        player = Player(name,availability[name])
        player.addContactInfo(contacts)
        player.checkQual(availability[name])
        roster_dict[name] = player
    return roster_dict


def buildMatches(schedule,locations):
    # build match schedule
    match_list = []
    for info in schedule:
        match = Match(info)
        match.addLocation(locations)
        match_list.append(match)
    return match_list


def sent(emails,player,email,reCheck=False):
    if reCheck:
        sheetsService = setupGoogleAPI('sheets','v4')
        emails = getEmails(sheetsService,getKey('privateDocId'))
    return emails[player][email]


def setSent(player,email,status='Yes'):
    player = player.replace(', ','-')
    request_private = { 'function': 'updatePrivateSheet', 'parameters': [player,email,status] }
    response_private = scriptService.scripts().run(body=request_private,scriptId=getKey('privateScriptId')).execute()


def getKey(keyName):
    keyFile = open('keys.txt','r')
    keyList = keyFile.readlines()
    for key in keyList:
        if key.find('#')>=0: continue
        if key.find(keyName)>=0:
            return key[(len(keyName)+1):-1] if key[-1]=='\n' else key[(len(keyName)+1):]
    return ''


def ratePlayers(available):
    '''Implementation of an algorithm to place players in order of importance.'''
    orderedPlayers = []

    for player in available:
        # "dumb" algorithm is to just take the first n
        orderedPlayers.append(player)

    return orderedPlayers

if __name__ == '__main__':
    # AutoCaptainMod Test Procedure
    sheetsService = AutoCaptainMod.setupGoogleAPI('sheets','v4','/u6/cdb3753/Documents/AutoCaptain/')
    scriptService = AutoCaptainMod.setupGoogleAPI('script','v1','/u6/cdb3753/Documents/AutoCaptain/')
    schedule,availability = AutoCaptainMod.getAvailability(sheetsService)
    emails = AutoCaptainMod.getEmails(sheetsService)
    contacts = AutoCaptainMod.getContacts(sheetsService)
    locations = AutoCaptainMod.getLocations(sheetsService)
    roster = AutoCaptainMod.buildRoster(availability,contacts)
    matches = AutoCaptainMod.buildMatches(schedule,availability,locations)
    toWhom = roster['Burton, Chuck']
    whichMatch = matches[1]
    html = MailHtml.writeAvailabilityMail(toWhom, whichMatch, getKey('publicWebId'))
    MailHtml.sendMail('test availability',html,[toWhom])
    html = MailHtml.writeLineupMail([toWhom],whichMatch)
    MailHtml.sendMail('test lineup',html,[toWhom])
    if toWhom.address != '':
        html = MailHtml.writeDirectionsMail(toWhom,whichMatch,getKey('mapKey'))
        MailHtml.sendMail('test location',html,[toWhom])

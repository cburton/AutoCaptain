// Google Script Functions

function doGet(e) {
  var nameString = e.parameter.name;
  var matchString = e.parameter.match;
  var valueString = e.parameter.value;
  return updatePublicSheet(nameString,matchString,valueString);
}

function updatePublicSheet(nameString,matchString,valueString) {
  // put default values for testing
  nameString = nameString||'Burton-Chuck';
  matchString = matchString||'5-20-2018';
  valueString = valueString||'Available';
  
  // convert name format
  nameString = nameString.replace(/-/g, ', ');
  matchString = matchString.replace(/-/g, '/');
  
  var sheet = SpreadsheetApp.getActiveSheet();
  var datarange = sheet.getDataRange();
  var data = datarange.getValues();
  var nRow = datarange.getNumRows();
  var nCol = datarange.getNumColumns();
  
  var name=0; // find the row corresponding to that person
  while ( name<nRow && data[name][0]!=nameString ) {
    name++;
  }
  
  var match = 0; // find the column corresponding to that match
  while ( match<nCol && data[3][match]!=matchString ) {
    Logger.log(data[3][match])
    match++;
  }
  
  // set the status as available
  sheet.getRange(name+1,match+1).setValue(valueString);
  return HtmlService.createHtmlOutput('Availability successfully updated.');
}


function updatePrivateSheet(nameString,emailString,valueString) {
  // put default values for testing
  nameString = nameString||'Burton-Chuck';
  emailString = emailString||'Initial';
  valueString = valueString||'No';
  
  // convert name format
  nameString = nameString.replace(/-/g, ', ');
  
  var sheet = SpreadsheetApp.getActiveSheet();
  var datarange = sheet.getDataRange();
  var data = datarange.getValues();
  var nRow = datarange.getNumRows();
  var nCol = datarange.getNumColumns();
  
  var name=0; // find the row corresponding to that person
  while ( name<nRow && data[name][0]!=nameString ) {
    name++;
  }
  
  var email = 0; // find the column corresponding to that email
  while ( email<nCol && data[0][email]!=emailString ) {
    email++;
  }
  
  // set the status as available
  sheet.getRange(name+1,email+1).setValue(valueString);
}
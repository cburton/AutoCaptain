import AutoCaptainMod
import MailHtml
from datetime import datetime,timedelta

groups = [6]
initialDay = 0
confirmDay = 6
reminderDay = 1
directionsDay = 1
needDay = 4


def scheduleMatches(matches,roster,schedule,availability):
    for i,match in enumerate(matches):

        # get available players
        match.availableNames = [name for name in availability if availability[name][i]=='Available']
        match.availablePlayers = [roster[player] for player in roster if roster[player].name in match.availableNames]

        # check for enough players
        match.sufficient = True
        for group in groups:

            # if not enough players, schedule those that you can and remind the others
            if len(match.availablePlayers)<group:
                match.sufficient = False
                match.scheduledPlayers = match.availablePlayers

            # if enough, prioritize them and schedule the highest algorithm-ranked players
            else:
                match.scheduledPlayers = AutoCaptainMod.ratePlayers(match.availablePlayers)[:group]

        # send emails for this match
        matchEmails(match)


def matchEmails(match):
    now = datetime.now()
    deltaT = match.dt-now

    doConfirm = timedelta(days=confirmDay-1)<deltaT<timedelta(days=confirmDay)
    doReminder = timedelta(days=reminderDay-1)<deltaT<timedelta(days=reminderDay)
    doDirections = timedelta(days=directionsDay-1)<deltaT<timedelta(days=directionsDay)
    doNeed = timedelta(days=needDay-1)<deltaT<timedelta(days=needDay) and not match.sufficient

    # print('deciding action for match on',match.date)

    # if doConfirm:
        # print('Sending availability mail.')
        # html = MailHtml.writeAvailabilityMail(match.scheduledPlayers, match, AutoCaptainMod.getKey('publicWebId'))
        # MailHtml.sendMail('Test Availability',html,match.scheduledPlayers)

    if doReminder:
        print('Sending reminder mail.')
        html = MailHtml.writeLineupMail(match.scheduledPlayers,match)
        MailHtml.sendMail('Test Lineup',html,match.scheduledPlayers)
        
    # if doDirections:
    #     print('Sending directions mail.')
    #     html = MailHtml.writeDirectionsMail(match.scheduledPlayers,match,AutoCaptainMod.getKey('mapKey'))
    #     MailHtml.sendMail('Test Directions',html,match.scheduledPlayers)

    # if doNeed:
        # print('Sending need mail.')

sheetsService = AutoCaptainMod.setupGoogleAPI('sheets','v4','/u6/cdb3753/Documents/AutoCaptain/')
scriptService = AutoCaptainMod.setupGoogleAPI('script','v1','/u6/cdb3753/Documents/AutoCaptain/')

schedule,availability = AutoCaptainMod.getAvailability(sheetsService)
emails = AutoCaptainMod.getEmails(sheetsService)
contacts = AutoCaptainMod.getContacts(sheetsService)
locations = AutoCaptainMod.getLocations(sheetsService)
roster = AutoCaptainMod.buildRoster(availability,contacts)
matches = AutoCaptainMod.buildMatches(schedule,locations)

scheduleMatches(matches,roster,schedule,availability)

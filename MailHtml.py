#!/bin/python
"""
@name: MailHtml.py
@author: Charles D. Burton
@email: burton@utexas.edu
@date: 21 May 2018
@description: Module to handle the various html requests for writing emails.
"""

def sendMail(subject,message,recipients):
    import smtplib
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.header import Header
    from email.utils import formataddr

    # me = formataddr((str(Header('Leon AutoCaptain', 'utf-8')), 'leon.autocaptain@gmail.com'))
    me = 'burton@utexas.edu'
    them = ''
    for rec in recipients:
        them += rec.email+','

    # Create message container with MIME type alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = me
    msg['To'] = them

    # Record the MIME html.
    text = MIMEText(message, 'html')
    msg.attach(text)

    # Send the message via local SMTP server.
    s = smtplib.SMTP('localhost')
    s.sendmail(me, them, msg.as_string())
    s.quit()


def writeAvailabilityMail(recipient,match,scriptId):
    # HTML keywords do not allow commas or slashes
    # replace them temporarily with '-'. The web app will convert it back.
    nameBad = recipient.name
    name = nameBad.replace(', ','-')
    dateBad = match.date
    date = dateBad.replace('/','-')

    addr = 'https://script.google.com/macros/s/'+scriptId+'/exec?name='+name+'&match='+date+'&value='
    yes = addr+'Available'
    no  = addr+'No'

    html  = '<html><body><p>'
    html += 'Dear '+recipient.firstName+', <br><br>'
    html += 'Our team has a match scheduled for '+match.time+' on '+match.date+' at '+match.location+'. '
    html += 'Please confirm your availability.<br><br>'
    html += '<a href="'+yes+'">Available</a> or '
    html += '<a href="'+no+'">Unavailable</a>?'
    html += '<br><br>Kindest regards, <br>Leon AutoCaptain'
    html += '</p></body></html>'
    return html


def writeLineupMail(recipients,match):
    html  = '<html><body><p>'
    html += 'Dear All, <br><br>'
    html += 'This is a friendly reminder of our team\'s match scheduled for '+match.time+' tomorrow ('+match.date+') at '+match.location+'. '
    html += 'The following players are scheduled to play: <br><br>'
    for recipient in recipients:
        html += '&nbsp;&nbsp;&nbsp;&nbsp;'+recipient.name+'<br>'
    html += '<br>See y\'all there!<br><br>Kindest regards, <br>Leon AutoCaptain'
    html += '</p></body></html>'
    return html


def writeDirectionsMail(recipient,match,gKey):
    from datetime import datetime,timedelta
    import GoogleMaps

    mapLink = GoogleMaps.getDirections(recipient,match)
    travelTime = GoogleMaps.getDuration(gKey,recipient,match)*1.1

    matchTime = datetime.strptime(match.date_time,'%I:%M %p %m/%d/%Y')
    leaveTime = datetime.strftime(matchTime - timedelta(seconds=travelTime), '%I:%M %p')

    html  = '<html><body><p>'
    html += 'Dear '+recipient.firstName+',<br><br>'
    html += 'Tomorrow\'s match is at '+match.location+'. Google Maps suggests leaving no later than '
    html += leaveTime+'. Please click <a href="'+mapLink+'">here</a> for directions.'
    html += '<br><br>Kindest regards, <br>Leon AutoCaptain'
    html += '</p></body></html>'
    return html


def writeNeedMail(recipients,match):
    html  = '<html><body><p>'
    html += 'Dear All, <br><br>'
    html += 'We have a match on '+match.date+' against '+match.opponent+', and our team is in need of players. '
    html += 'Please update your availability on the GoogleDoc if you are available.'
    html += '<br><br>Kindest regards, <br>Leon AutoCaptain'
    html += '</p></body></html>'
    return html


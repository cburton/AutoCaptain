# Description
This is a little package to automate the captaining of a tennis team. The program runs off a nightly cron job and communicates with players by email.

At the beginning of the season, it emails the whole team with a Google form at the start of the season to get availability and contact information. Then, each week it sends an email to players listed as available asking to confirm their availability. The next night, it attempts to create a lineup. Then, if the lineup is complete, it sends the proposed lineup to the captain, who can make changes on the google doc. If the linup is incomplete, it notifies the captain and sends notifications to players listed as unavailable and asks them to play. Finally, the day before, it sends an email to all scheduled players with a reminder of the match, a suggested departure time, and directions from their home to the match location.

Each email is recorded to ensure that no player receives the same email more than once. To re-send an email, the administrator may change the Google Sheet manually.

The gmail account `leon.autocaptain@gmail.com` has been reserved for this program, but is often picked up by spam filters, so it is recommended to use a `.edu` address to avoid this.

For a more detailed description, please read my blog post at http://cburton.web.cern.ch/cburton/blog-autocaptain.html.

# Setup and Installation
1. The first step is to setup a Google Cloud platform (https://console.cloud.google.com/). Once the project is created, choose `APIs & Services > Library` in the hamburger menu. Use the search tool to find and enable "Google Sheets API", "Apps Script API", and "Directions API". 

2. Then, one needs to create two google docs. The public one contains the schedule and availability data. This is public to everyone on the team. The other private document contains the contact info (name, email, gender, level, and mailing address) which is stored privately, email history, and match addresses. (This code is very dependent on indices of the Google Sheets, so the format should not be changed unless you know what you are doing.)

3. Perform the following actions for each google doc. Choose `Tools > Script Editor`. Copy the appropriate code from `google_scripts.js` into `Code.gs`. Choose `Resources > Cloud Platform project...`. In the "Change Project" box, enter the project number of the cloud project that was created in Step 1.

4. Back in the cloud console, setup the appropriate credentials. Navigate to `APIs & Services > Credentials`. Using "Create credentials", generate an API key. Additionally, create an OAuth 2.0 client IDs in the same way. When asked for "Application type", choose "Other". Download the .json file that was generated, and copy it to the running directory as `client_secret.json`.

5. The `keys.txt` file needs to be edited with the various keys that have been generated in the above steps. Most of them can just be copied from the url's of the relevant Google Doc/Sheet.

6. Run a quick test with 
```bash
python AutoCaptainMod.py --noauth_local_webserver
```
You will be asked the first time to authenticate with your google login. (So you may want to have X forwarding set up when you do this step.)

7. Finally, add the following to your `crontab -e` file.
```bash
30 8 * * * /bin/bash /u6/cdb3753/Documents/AutoCaptain/cron_script.sh
```
To run daily at 08:30 on the server's wall time. Adjust the notification time as desired.

Please see license.txt for licensing information.
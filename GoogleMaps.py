#!/bin/python
"""
@name: GoogleMaps.py
@author: Charles D. Burton
@email: burton@utexas.edu
@date: 21 May 2018
@description: Interfaces with Google Maps API to get directions.
"""
import googlemaps
from datetime import datetime,timedelta


def getDuration(gKey,recipient,match):
    matchTime = datetime.strptime(match.date_time,'%I:%M %p %m/%d/%Y')

    gmaps = googlemaps.Client(key=gKey)
    directions_result = gmaps.directions(recipient.address,
                                         match.address,
                                         mode='driving',
                                         avoid='tolls',
                                         arrival_time=matchTime)

    dt = directions_result[0]['legs'][0]['duration']['value']
    return dt


def getDirections(recipient,match):
    matchTime = datetime.strptime(match.date_time,'%I:%M %p %m/%d/%Y')
    sinceEpoch = str(int((matchTime - datetime(1970, 1, 1)).total_seconds()))

    start = recipient.address.replace(' ','+')
    end = match.address.replace(' ','+')
    googleMapsLink = 'https://www.google.com/maps/dir/'+start+'/'+end+'/data=!4m5!4m4!2m3!6e1!7e2!8j'+sinceEpoch
    return googleMapsLink


# a main function for testing
if __name__ == '__main__':
    print('Testing Google Maps interface.')
    gKey = 'AIzaSyCZK3iieSrE7oFZmdjAfOEs0pvRLTCAukE'
    matchTime = datetime.strptime('9:00 AM 12/31/2018','%I:%M %p %m/%d/%Y')
    gmaps = googlemaps.Client(key=gKey)
    directions_result = gmaps.directions('3401 Red River Street Austin, TX 78705',
                                         '4608 Connelly Street Austin, TX 78751',
                                         arrival_time=matchTime)
    dt = directions_result[0]['legs'][0]['duration']['value']
    print(matchTime - timedelta(seconds=dt))